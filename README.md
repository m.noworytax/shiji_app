Project created for workshops carried out by Shiji: Automating the Future: DevOps in Practice

After using command docker-compose up -d project will generate 3 containers on ports:

- localhost:8000 - web application written in Django in Python
- localhost:9090 - prometheus
- loaclhost:3000 - grafana

The app shows data from API found on the Internet related to worldwide currencies and their exchange rates to Euro.

Prometheus is providing metrics for the app.

Grafana is there to show some metrics taken from Prometheus on simple dashboard.

The other objective of the workshops was to put the project into AWS. Unfortunately it didn't go as planned due to issues with connecting to Amazon cloud service. 