from django.shortcuts import render,HttpResponse, redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from app1.api_call import api_call

# Create your views here.
@login_required(login_url='login')



def HomePage(request):
    name_list = []
    eur_list = []
    curr_list = []
    name_list, eur_list = api_call()
    #help = api_call()
    #name_list = help(0)
    #eur_list = help(1)
    curr_list = zip(name_list, eur_list)
    context = {
        'curr': curr_list,

    }
    return render (request, 'home.html', context)

def SignupPage(request):
    if request.method=='POST':
        uname=request.POST.get('username')
        email=request.POST.get('email')
        pass1=request.POST.get('password1')
        pass2=request.POST.get('password2')

        if pass1!=pass2:
            messages.error(request, "Your password and confirm password are not the same !!!")
        else:

            my_user=User.objects.create_user(uname,email,pass1)
            messages.success(request, "You signed up successfully ^.^")
            my_user.save()
            return redirect('login')
            
    return render (request, 'signup.html')

def LoginPage(request):
    if request.method == 'POST':
        username=request.POST.get('username')
        pass1=request.POST.get('pass')
        user=authenticate(request,username=username,password=pass1)
        if user is not None:
            login(request,user)
            messages.success(request, f"You've successfully logged in: {request.user.username}")
            return redirect('home')
        else:
            messages.error(request, "Username or Password is incorrect !!!")
        
        

    return render (request, 'login.html')

def LogoutPage(request):
    logout(request)
    messages.success(request, "You've successfully logged out of the application")
    return redirect('login')

